/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <aerostack_msgs/ListOfBeliefs.h>
#include <behavior_pay_attention_to_qr_code.h>
#include <cstdio>
#include <droneMsgsROS/QRInterpretation.h>
#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <thread>

ros::NodeHandle *nh;

class BeliefsListener
{
public:
  ros::Subscriber all_beliefs_sub;

  bool belief_received;

  BeliefsListener() { belief_received = false; }

  void start()
  {
    all_beliefs_sub = nh->subscribe("/drone1/all_beliefs", 1, &BeliefsListener::allBeliefsCallback, this);
  }

  void allBeliefsCallback(const aerostack_msgs::ListOfBeliefs &message) { belief_received = true; }
};

TEST(BehaviorPayAttentionToQrCode, simplePayAttentionToQrCodeTest)
{
  std::cout << "START TEST" << std::endl;

  BeliefsListener bl;
  bl.start();

  ros::Duration(2).sleep();
  ros::Rate rate(30);

  aerostack_msgs::StartBehavior::Request msg;
  aerostack_msgs::StartBehavior::Response res;

  std::string behavior_path = "/drone1/behavior_pay_attention_to_qr_code/start";

  ros::ServiceClient behavior_cli = nh->serviceClient<aerostack_msgs::StartBehavior>(behavior_path);

  behavior_cli.call(msg, res);
  ros::Duration(2).sleep();
  int it = 0;

  while (!res.ack && it < 20)
  {
    ros::spinOnce();
    rate.sleep();
    it++;
  }

  ros::Publisher qr_interpretation_pub;
  qr_interpretation_pub = nh->advertise<droneMsgsROS::QRInterpretation>("/drone1/qr_interpretation", 1);
  system("rostopic info /drone1/qr_interpretation");
  droneMsgsROS::QRInterpretation test_msg;
  test_msg.message = "test";
  qr_interpretation_pub.publish(test_msg);

  behavior_cli.call(msg, res);
  ros::Duration(2).sleep();

  it = 0;

  while (!bl.belief_received && it < 20)
  {
    ros::spinOnce();
    rate.sleep();
    it++;
  }

  EXPECT_TRUE(bl.belief_received);
}

/*--------------------------------------------*/
/*  Main  */

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;
  std::cout << "WAIT WHILE LAUNCHING AEROSTACK" << std::endl;
  system("bash "
         "$AEROSTACK_STACK/stack/executive_system/behavior_management_system/behavior_library/"
         "behavior_pay_attention_to_qr_code/src/test/test.sh");
  ros::Duration(5).sleep();

  return RUN_ALL_TESTS();
}
