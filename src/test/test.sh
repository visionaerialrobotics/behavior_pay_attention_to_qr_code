#!/bin/bash

#---------------------------------------------------------------------------------------------
# Input arguments
#---------------------------------------------------------------------------------------------
NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_IP=$3
DRONE_WCHANNEL=$4

#---------------------------------------------------------------------------------------------
# Default values for arguments
#
# This code checks the existence of arguments as it is explained at:
#      http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
#---------------------------------------------------------------------------------------------
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
    # Argument 2 is empty
    . ${AEROSTACK_STACK}/setup.sh
    OPEN_ROSCORE=1
  else
   . ${AEROSTACK_STACK}/setup.sh $2
fi

if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
    # Argument 1 empty
    echo "-Setting droneId = 1"
    NUMID_DRONE=1
  else
    echo "-Setting droneId = $1"
fi

if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
    # Argument 3 is empty
    echo "-Setting droneIp = 192.168.1.1"
    DRONE_IP=192.168.1.1
  else
    echo "-Setting droneIp = $3"
  fi

if [ -z $DRONE_WCHANNEL ] # Check if NUMID_DRONE is NULL
  then
    # Argument 4 is empty
    echo "-Setting droneChannel = 6"
    DRONE_WCHANNEL=6
  else
    echo "-Setting droneChannel = $4"
  fi

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Visual Markers Localizer                                                                    ` \
`# Finds and recognizes Visual Markers                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Visual Marker Localizer" --command "bash -c \"
roslaunch droneVisualMarkersLocalizerROSModule droneVisualMarkersLocalizerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${DRONE_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Communication manager                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Communication Manager" --command "bash -c \"
roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process monitor                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process Monitor"	--command "bash -c \"
roslaunch process_monitor_process process_monitor.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
  `#---------------------------------------------------------------------------------------------` \
  `# Process manager                                                                        ` \
  `#---------------------------------------------------------------------------------------------` \
  --tab --title "Process manager" --command "bash -c \"
  roslaunch process_manager_process process_manager_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE \
      drone_id:=$NUMID_DRONE \
      my_stack_directory:=${AEROSTACK_STACK};
  exec bash\""  \
  `#---------------------------------------------------------------------------------------------` \
  `# Belief Manager                                                                              ` \
  `#---------------------------------------------------------------------------------------------` \
  --tab --title "Belief Manager" --command "bash -c \"
  roslaunch belief_manager_process belief_manager_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE \
      drone_id:=$NUMID_DRONE \
      my_stack_directory:=${AEROSTACK_STACK};
  exec bash\""  \
  `#---------------------------------------------------------------------------------------------` \
  `# Belief Updater                                                                              ` \
  `#---------------------------------------------------------------------------------------------` \
  --tab --title "Belief Updater" --command "bash -c \"
  roslaunch belief_updater_process belief_updater_process.launch --wait \
      drone_id_namespace:=drone$NUMID_DRONE \
      drone_id:=$NUMID_DRONE \
      my_stack_directory:=${AEROSTACK_STACK};
  exec bash\""  &

xfce4-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behaviors                                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Pay Attention to QR Code                                                           ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Pay Attention to QR Code" --command "bash -c \"
roslaunch behavior_pay_attention_to_qr_code behavior_pay_attention_to_qr_code.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    refresh_rate:=5;
exec bash\"" &

