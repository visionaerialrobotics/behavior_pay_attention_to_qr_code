# behavior_pay_attention_to_qr_code

**Purpose**: The robot pays attention to QR codes. Each recognized code is stored as a belief with the following format: object(X, qr_code), code(X, Y), visible(X) where X is the object identifier (generated automatically) and Y is the recognized QR code (string). The predicate visible(X) is deleted when the QR code is not observed. We assume that the objects with QR codes are static (they don't move in the environment). We assume that there are not different objects with the same QR code.

**Type of behavior:** Recurrent.

# Contributors

**Code maintainer:** Ra�l Cruz

**Authors:** Alberto Camporredondo Portela
